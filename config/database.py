import os   #interactua con el so, majeno de rutas de archivos
from sqlalchemy import create_engine #conexion al motor de la bd
#Configurar y fabricar sesiones de la bd
from sqlalchemy.orm.session import sessionmaker 
from sqlalchemy.ext.declarative import declarative_base

sqlite_file_name = "../database.sqlite"

#lectura del directorio actual
base_dir = os.path.dirname(os.path.realpath(__file__))

#creamos la url de la bd con las dos variables
database_url = f"sqlite:///{os.path.join(base_dir, sqlite_file_name)}"

#Crea el motor de conexion a la bd
engine = create_engine(database_url, echo= True)
#fabirca de sesiones vinculada al motor
session = sessionmaker(bind= engine)
#crea una base para modelos declarativos
Base = declarative_base()

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind = engine)   