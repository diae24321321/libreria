from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from config.database import Base

class Book(Base):
    __tablename__ = "books"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    title = Column(String, index=True)
    author = Column(String, index=True)
    year = Column(Integer)
    category_id = Column(Integer, ForeignKey('categories.id'))
    pages = Column(Integer)

    category = relationship("Category", back_populates="books")

    def _repr_(self):
        return f"<Book(title={self.title}, author={self.author}, year={self.year}, pages={self.pages})>"