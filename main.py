from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import book, category
from middlewares.error_handler import ErrorHandler
from config.database import engine, Base

# Crear la aplicación FastAPI
app = FastAPI(title="Biblioteca de la Maravilla, ya quedo esta madre", version="0.0.1")
  



# Configurar CORS
origins = [
    "http://localhost:3000",
      "http://165.22.16.179:5000"  # Añadir otros orígenes si es necesario
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Incluir routers de diferentes recursos
app.include_router(book.router, tags=["Books"])
app.include_router(category.router, tags=["Category"])

Base.metadata.create_all(bind=engine) # Crea archivo de db que contiene las tablas

@app.get("/", tags=["Root"])
async def root():
    return {"message": "Bienvenido a la Biblioteca de la Maravilla!"}
