from typing import List
from fastapi import APIRouter, HTTPException, Depends
from models.book import Book
from models.category import Category
from sqlalchemy.orm import Session
from pydantic import BaseModel
from config.database import SessionLocal
#from schemas.category import CategorySchema

router = APIRouter()

class BookCreate(BaseModel):
    title: str
    author: str
    year: int
    category_name: str
    pages: int

class BookUpdate(BaseModel):
    title: str = None
    author: str = None
    year: int = None
    category_name: str = None
    pages: int = None

class CategoryModel(BaseModel):
    id: int
    name: str

    class Config:
        orm_mode = True

class BookModel(BaseModel):
    id: int
    title: str
    author: str
    year: int
    pages: int
    category: CategoryModel  # Nested relationship

    class Config:
        orm_mode = True     

class CategoryCreate(BaseModel):
    name: str

class CategoryUpdate(BaseModel):
    name: str

# Dependencia to get database session
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@router.get("/categories/", response_model=List[CategoryModel])
def get_all_categories(db: Session = Depends(get_db)):
    categories = db.query(Category).all()
    return categories

@router.get("/categories/{category_id}", response_model=CategoryModel)
def get_category(category_id: int, db: Session = Depends(get_db)):
    category = db.query(Category).filter(Category.id == category_id).first()
    if category is None:
        raise HTTPException(status_code=404, detail="Category not found")
    return category

@router.post("/categories/", response_model=dict)
def create_category(category: CategoryCreate, db: Session = Depends(get_db)):
    existing_category = db.query(Category).filter(Category.name == category.name).first()
    if existing_category:
        return {"message": "Category already exists"}
    new_category = Category(name=category.name)
    db.add(new_category)
    db.commit()
    db.refresh(new_category)
    return {"message": "Category created successfully", "category_id": new_category.id}

@router.put("/categories/{category_id}", response_model=dict)
def update_category(category_id: int, category: CategoryUpdate, db: Session = Depends(get_db)):
    existing_category = db.query(Category).filter(Category.id == category_id).first()
    if existing_category is None:
        raise HTTPException(status_code=404, detail="Category not found")
    if existing_category.books:
        # Aquí suponemos que deseas continuar con la actualización a pesar de tener libros asociados
        for book in existing_category.books:
            book.category_name = category.name  # Asumiendo que hay un campo 'category_name' en los libros
        db.add_all(existing_category.books)  # Agrega todos los libros modificados a la sesión de la base de datos
    existing_category.name = category.name
    db.commit()
    return {"message": "Category updated successfully"}


@router.delete("/categories/{category_id}", response_model=dict)
def delete_category(category_id: int, db: Session = Depends(get_db)):
    category = db.query(Category).filter(Category.id == category_id).first()
    if not category:
        raise HTTPException(status_code=404, detail="Category not found")

    if category.books:  # Check if the category has associated books
        raise HTTPException(status_code=400, detail="Cannot delete category with associated books")

    db.delete(category)
    db.commit()
    return {"message": "Category deleted successfully"}
