from typing import List
from fastapi import APIRouter, HTTPException, Depends
from models.book import Book
from models.category import Category
from sqlalchemy.orm import Session, joinedload
from pydantic import BaseModel
from config.database import SessionLocal

class BookCreate(BaseModel):
    title: str
    author: str
    year: int
    category_name: str
    pages: int

class BookUpdate(BaseModel):
    title: str = None
    author: str = None
    year: int = None
    category_name: str = None
    pages: int = None

class CategoryModel(BaseModel):
    id: int
    name: str

    class Config:
        orm_mode = True

class BookModel(BaseModel):
    id: int
    title: str
    author: str
    year: int
    pages: int
    category: CategoryModel  # Nested relationship

    class Config:
        orm_mode = True     

class CategoryCreate(BaseModel):
    name: str

class CategoryUpdate(BaseModel):
    name: str

router = APIRouter()

# Dependencia para obtener la sesión de la base de datos
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

# Endpoints de libros
@router.get("/books/", response_model=List[BookModel])
def get_all_books(db: Session = Depends(get_db)):
    books = db.query(Book).options(joinedload(Book.category)).all()
    return books

@router.get("/books/{book_id}", response_model=BookModel)
def get_book(book_id: int, db: Session = Depends(get_db)):
    book = db.query(Book).filter(Book.id == book_id).options(joinedload(Book.category)).first()
    if book is None:
        raise HTTPException(status_code=404, detail="Book not found")
    return book

@router.post("/books/", response_model=dict)
def create_book(book: BookCreate, db: Session = Depends(get_db)):
    category = db.query(Category).filter(Category.name == book.category_name).first()
    if not category:
        return {"message": "Category does not exist, cannot create book"}

    new_book = Book(title=book.title, author=book.author, year=book.year, category_id=category.id, pages=book.pages)
    db.add(new_book)
    db.commit()
    db.refresh(new_book)
    return {"message": "Book added successfully", "book_id": new_book.id}

@router.put("/books/{book_id}", response_model=dict)
def update_book(book_id: int, book: BookUpdate, db: Session = Depends(get_db)):
    existing_book = db.query(Book).filter(Book.id == book_id).first()
    if not existing_book:
        raise HTTPException(status_code=404, detail="Book not found")
    if book.title: existing_book.title = book.title
    if book.author: existing_book.author = book.author
    if book.year: existing_book.year = book.year
    if book.category_name:
        category = db.query(Category).filter(Category.name == book.category_name).first()
        if not category:
            raise HTTPException(status_code=400, detail="Category does not exist")
        existing_book.category_id = category.id
    if book.pages: existing_book.pages = book.pages
    db.commit()
    return {"message": "Book updated successfully"}

@router.delete("/books/{book_id}", response_model=dict)
def delete_book(book_id: int, db: Session = Depends(get_db)):
    book = db.query(Book).filter(Book.id == book_id).first()
    if book is None:
        raise HTTPException(status_code=404, detail="Book not found")
    db.delete(book)
    db.commit()
    return {"message": "Book deleted successfully"}
